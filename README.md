# caffe-models

In this repository you can find a collection of pretrained caffe models.

Network                  | Size     | Source
------------------------ | -------- | ------
VGG16                    | 527.79MB | [University of Oxford](http://www.robots.ox.ac.uk/%7Evgg/research/very_deep/)
VGG19                    | 548.05MB | [University of Oxford](http://www.robots.ox.ac.uk/%7Evgg/research/very_deep/)
ResNet-18                | 44.65MB  | [HolmesShuan/ResNet-18-Caffemodel-on-ImageNet](https://github.com/HolmesShuan/ResNet-18-Caffemodel-on-ImageNet)
ResNet-50                | 97.72MB  | [KaimingHe/deep-residual-networks](https://github.com/KaimingHe/deep-residual-networks)
ResNet-101               | 170.39MB | [KaimingHe/deep-residual-networks](https://github.com/KaimingHe/deep-residual-networks)
ResNet-152               | 230.26MB | [KaimingHe/deep-residual-networks](https://github.com/KaimingHe/deep-residual-networks)
DenseNet-121             | 30.81MB  | [shicai/DenseNet-Caffe](https://github.com/shicai/DenseNet-Caffe)
DenseNet-161             | 110.32MB | [shicai/DenseNet-Caffe](https://github.com/shicai/DenseNet-Caffe)
DenseNet-169             | 54.65MB  | [shicai/DenseNet-Caffe](https://github.com/shicai/DenseNet-Caffe)
DenseNet-201             | 77.31MB  | [shicai/DenseNet-Caffe](https://github.com/shicai/DenseNet-Caffe)
Inception-v4             | 163.13MB | [soeaver/caffe-model](https://github.com/soeaver/caffe-model/tree/master/cls)
Inception-ResNet-v2      | 213.42MB | [soeaver/caffe-model](https://github.com/soeaver/caffe-model/tree/master/cls)
MobileNet                | 16.24MB  | [shicai/MobileNet-Caffe](https://github.com/shicai/MobileNet-Caffe)
MobileNet-v2             | 13.53MB  | [shicai/MobileNet-Caffe](https://github.com/shicai/MobileNet-Caffe)
SqueezeNet_v1.0          | 4.77MB   | [DeepScale/SqueezeNet](https://github.com/DeepScale/SqueezeNet)
SqueezeNet_v1.0_DSD      | 4.77MB   | [songhan/SqueezeNet-DSD-Training](https://github.com/songhan/SqueezeNet-DSD-Training)
SqueezeNet_v1.0_Residual | 4.77MB   | [songhan/SqueezeNet-Residual](https://github.com/songhan/SqueezeNet-Residual)
SqueezeNet_v1.1          | 4.72MB   | [DeepScale/SqueezeNet](https://github.com/DeepScale/SqueezeNet)
